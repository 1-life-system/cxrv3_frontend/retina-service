import Vue from "vue";
import Vuex from "vuex";

Vue.use(Vuex);

export default new Vuex.Store({
  state: {
    disease: [],
    upload_id: [],
  },
  mutations: {
    setDisease(state, data) {
      state.disease = data
    },
    setUpload_id(state, data) {
      state.upload_id = data
    }
  },
  actions: {},
  modules: {},
});
