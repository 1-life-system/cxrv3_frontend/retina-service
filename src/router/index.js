import Vue from 'vue'
import VueRouter from 'vue-router'
import Navbar from '@/components/Navbar.vue'
import Upload from '@/views/children/Upload.vue'
import History from '@/views/children/History.vue'
import Dashboard from '@/views/children/Dashboard.vue'
import Display from '@/views/children/Display.vue'
import VueCookies from 'vue-cookies'

Vue.use(VueRouter)

const routes = [
  {
    path: '/navbar',
    name: 'Navbar',
    component: Navbar,
    meta: { viewer: true },
    children: [
      {
        path: '/upload',
        name: 'Upload',
        meta: { user: true },
        component: Upload
      },
      {
        path: '/history',
        name: 'History',
        meta: { viewer: true },
        component: History
      },
      {
        path: '/dashboard',
        name: 'Dashboard',
        meta: { user: true },
        component: Dashboard
      },
      {
        path: '/display',
        name: 'Display',
        meta: { user: true },
        component: Display
      },
    ]
  }
]

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
})

router.beforeEach((to, from, next) => {
  if (to.matched.some(record => record.meta.viewer)) {
    const checkcookie = VueCookies.get('service').package_use
    console.log("check-cookies", checkcookie)
    const findRetina = checkcookie.find(element => element == "retina");
    // console.log('if');
    if (!findRetina) {
      window.location.href = "http://localhost:8080/login";
    } else if (to.matched.some(record => record.meta.user)) {
      const checkcookie = VueCookies.get('service')
      const findRetina = checkcookie.package_use.find(element => element == "retina");
      if (findRetina) {
        if (checkcookie.data.role === 'user') {
          next()
        } else {
          next({ name: 'History' })
        }
      }
    } else {
      next()
    }
  } else {
    // console.log('else');
    next()
  }
})

export default router
